<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Repository;

/**
 * Interface FactoryInterface.
 */
interface FactoryInterface
{
    /**
     * @param string      $path
     * @param string|null $remote
     *
     * @return RepositoryInterface
     */
    public function create(string $path, ?string $remote): RepositoryInterface;
}
