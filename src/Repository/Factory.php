<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Repository;

use GitWrapper\GitException;
use GitWrapper\GitWrapper;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Factory.
 */
final class Factory implements FactoryInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var GitWrapper
     */
    private $wrapper;

    /**
     * Factory constructor.
     *
     * @param GitWrapper|null      $wrapper
     * @param LoggerInterface|null $logger
     */
    public function __construct(GitWrapper $wrapper = null, LoggerInterface $logger = null)
    {
        $this->wrapper = $wrapper ?: new GitWrapper();
        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Safe\Exceptions\FilesystemException
     */
    public function create(string $path, ?string $remote): RepositoryInterface
    {
        try {
            $strategy = $remote ? new DefaultRemoteStrategy($this->wrapper, $path, $remote, $this->logger) : new NoRemoteStrategy($this->wrapper, $path, $this->logger);
            $workingCopy = $strategy->prepare();
            $strategy->reset();

            return new Repository($workingCopy, $strategy, $this->logger);
        } catch (GitException $gitException) {
            throw \Irstea\MakeShim\Exception\GitException::fromGitWrapper($gitException);
        }
    }
}
