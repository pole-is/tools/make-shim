<?php
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Repository;

use GitWrapper\GitException;
use GitWrapper\GitWorkingCopy;
use GitWrapper\GitWrapper;
use Psr\Log\LoggerInterface;

final class DefaultRemoteStrategy extends NoRemoteStrategy
{
    /**
     * @var string
     */
    private $remote;

    public function __construct(GitWrapper $wrapper, string $path, string $remote, LoggerInterface $logger = null)
    {
        parent::__construct($wrapper, $path, $logger);
        $this->remote = $remote;
    }

    protected function doPrepare(): GitWorkingCopy
    {
        if (!$this->isInited()) {
            try {
                return $this->wrapper->cloneRepository($this->remote, $this->path);
            } catch (GitException $gitException) {
                $this->logger->warning("Could not clone from {$this->remote}: {$gitException->getMessage()}", ['exception' => $gitException]);
            }
        }
        $workingCopy = parent::doPrepare();

        $remotes = $workingCopy->getRemotes();
        if (empty($remotes['origin'])) {
            $workingCopy->addRemote('origin', $this->remote);
            $this->logger->debug("Added remote 'origin': {$this->remote}");
        } elseif ($remotes['origin']['fetch'] !== $this->remote) {
            $workingCopy->remote('set-url', 'origin', $this->remote);
            $this->logger->debug("Set remote 'origin': {$this->remote}");
        }

        return $workingCopy;
    }

    public function reset(): void
    {
        $workingCopy = $this->prepare();

        $this->logger->debug('Fetching from origin');
        $workingCopy->fetch('origin', 'master', '--tags', '--prune', '--force');

        $this->logger->debug('Resetting to origin/master');
        $workingCopy->reset('--hard', 'origin/master');
    }

    public function push(array $refs): bool
    {
        $workingCopy = $this->prepare();
        $workingCopy->push('--force', $this->remote, ...$refs);
        $this->logger->notice("Successfully pushed to $this->remote: " . implode(', ', $refs));
        $workingCopy->run('prune');

        return true;
    }
}
