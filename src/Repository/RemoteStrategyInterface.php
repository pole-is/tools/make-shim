<?php
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Repository;

use GitWrapper\GitWorkingCopy;

interface RemoteStrategyInterface
{
    public function prepare(): GitWorkingCopy;

    public function reset(): void;

    public function push(array $refs): bool;
}
