<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Repository;

use GitWrapper\GitException;
use GitWrapper\GitWorkingCopy;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Repository.
 */
final class Repository implements RepositoryInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var GitWorkingCopy
     */
    private $workingCopy;

    /**
     * @var array<string,bool>
     */
    private $changedRefs = [];

    /**
     * @var RemoteStrategyInterface
     */
    private $remoteStrategy;

    /**
     * Repository constructor.
     *
     * @param GitWorkingCopy          $workingCopy
     * @param RemoteStrategyInterface $remoteStrategy
     * @param LoggerInterface|null    $logger
     */
    public function __construct(GitWorkingCopy $workingCopy, RemoteStrategyInterface $remoteStrategy, LoggerInterface $logger = null)
    {
        $this->workingCopy = $workingCopy;
        $this->logger = $logger ?: new NullLogger();
        $this->remoteStrategy = $remoteStrategy;
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectory(): string
    {
        return $this->workingCopy->getDirectory();
    }

    /**
     * {@inheritdoc}
     */
    public function hasTag(string $tag): bool
    {
        try {
            foreach ($this->workingCopy->tags()->getIterator() as $ref) {
                if ($tag === $ref) {
                    return true;
                }
            }

            return false;
        } catch (GitException $gitException) {
            throw \Irstea\MakeShim\Exception\GitException::fromGitWrapper($gitException);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasChangedRefs(): bool
    {
        return !empty($this->changedRefs);
    }

    /**
     * {@inheritdoc}
     */
    public function commitAndTag(string $tag, string $message): bool
    {
        try {
            $this->workingCopy->add('-A');
            if (!$this->workingCopy->hasChanges()) {
                return false;
            }

            $this->workingCopy->commit($message);
            $this->workingCopy->tag('-f', $tag);

            $this->changedRefs[$tag] = true;
            $this->changedRefs[$this->workingCopy->getBranches()->head()] = true;

            return true;
        } catch (GitException $gitException) {
            throw \Irstea\MakeShim\Exception\GitException::fromGitWrapper($gitException);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function push(): bool
    {
        try {
            if ($this->hasChangedRefs() && $this->remoteStrategy->push(array_keys($this->changedRefs))) {
                $this->changedRefs = [];

                return true;
            }

            return false;
        } catch (GitException $gitException) {
            throw \Irstea\MakeShim\Exception\GitException::fromGitWrapper($gitException);
        }
    }
}
