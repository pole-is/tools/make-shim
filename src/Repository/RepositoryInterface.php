<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Repository;

use Irstea\MakeShim\Exception\GitException;

/**
 * Interface RepositoryInterface.
 */
interface RepositoryInterface
{
    /**
     * @param string $tag
     *
     * @throw GitException
     *
     * @return bool
     */
    public function hasTag(string $tag): bool;

    /**
     * @return bool
     */
    public function hasChangedRefs(): bool;

    /**
     * @param string $tag
     * @param string $message
     *
     * @throws GitException
     *
     * @return bool
     */
    public function commitAndTag(string $tag, string $message): bool;

    /**
     * @return bool
     *
     * @throw GitException
     */
    public function push(): bool;

    /**
     * @return string
     */
    public function getDirectory(): string;
}
