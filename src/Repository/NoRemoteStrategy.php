<?php
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Repository;

use GitWrapper\GitWorkingCopy;
use GitWrapper\GitWrapper;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use function Safe\mkdir;

class NoRemoteStrategy implements RemoteStrategyInterface
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var GitWrapper
     */
    protected $wrapper;

    /**
     * @var GitWorkingCopy|null
     */
    private $workingCopy;

    public function __construct(GitWrapper $wrapper, string $path, LoggerInterface $logger = null)
    {
        $this->wrapper = $wrapper;
        $this->path = $path;
        $this->logger = $logger ?: new NullLogger();
    }

    protected function isInited(): bool
    {
        return is_dir($this->path . '/.git');
    }

    /**
     * @throws \Safe\Exceptions\FilesystemException
     */
    public function prepare(): GitWorkingCopy
    {
        if (!$this->workingCopy) {
            $this->workingCopy = $this->doPrepare();
        }

        return $this->workingCopy;
    }

    /**
     * @throws \Safe\Exceptions\FilesystemException
     */
    protected function doPrepare(): GitWorkingCopy
    {
        if (!$this->isInited()) {
            $this->logger->debug("Creating a new repository: {$this->path}");
            mkdir($this->path, 0777, true);

            return $this->wrapper->init($this->path);
        }

        $this->logger->debug("Reusing existing repository: {$this->path}");

        return $this->wrapper->workingCopy($this->path);
    }

    public function reset(): void
    {
        $this->logger->debug('Resetting to master');
        $this->prepare()->reset('--hard');
    }

    public function push(array $refs): bool
    {
        $this->logger->info('Not pushing reference(s): ' . join(', ', $refs));

        return false;
    }
}
