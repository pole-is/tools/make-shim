<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Command;

use Irstea\MakeShim\Builder\BuilderInterface;
use Irstea\MakeShim\Packagist\PackageFilterInterface;
use Irstea\MakeShim\Packagist\PackagistInterface;
use Irstea\MakeShim\Repository\RepositoryInterface;
use Pimple\Container;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class BuildCommand.
 */
class BuildCommand extends Command
{
    /** @var string */
    private const NAME = 'build';
    /**
     * @var Container
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function __construct(Container $container)
    {
        parent::__construct(self::NAME);
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->addArgument('config', InputArgument::OPTIONAL, 'MakeShim file', 'make-shim.yaml')
            ->addOption('no-verify', '', InputOption::VALUE_NONE, 'Do not verify GPG signatures')
            ->addOption('no-remote', '', InputOption::VALUE_NONE, 'Do not pull from nor push to remote repository')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force rebuild of every versions');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->container['output'] = $output;
        $this->container['config-path'] = $input->getArgument('config');
        $this->container['no-verify'] = $input->getOption('no-verify');
        $this->container['no-remote'] = $input->getOption('no-remote');
        $force = (bool) $input->getOption('force');

        /** @var array $conf */
        $conf = $this->container['config'];

        /** @var LoggerInterface $logger */
        $logger = $this->container['logger'];

        /** @var PackagistInterface $packagist */
        $packagist = $this->container['packagist'];

        /** @var PackageFilterInterface $filter */
        $filter = $this->container['package-filter'];

        $packages = $filter->filter(
            $packagist->enumerateVersions($conf['package']['name'])
        );

        $logger->notice(sprintf('Selected %d version(s)', count($packages)));

        if (!$packages) {
            return 0;
        }

        /** @var RepositoryInterface $repository */
        $repository = $this->container['repository'];

        /** @var BuilderInterface $builder */
        $builder = $this->container['builder'];

        foreach ($packages as $package) {
            try {
                $tagName = $package->getVersion();
                if ($repository->hasTag($tagName)) {
                    if (!$force) {
                        $logger->notice("Version $tagName already exists, skipping");
                        continue;
                    }
                    $logger->notice("Version $tagName already exists, forcibly rebuild it");
                }

                $builder->build($package);

                if (!$repository->commitAndTag(
                    $tagName,
                    sprintf('%s v%s', $package->getName(), ltrim($tagName, 'v'))
                )) {
                    $logger->debug("no changes for $tagName");
                }
            } catch (\Exception $ex) {
                $logger->error(sprintf('Error building %s: %s', $package, $ex->getMessage()));
            }
        }

        if ($repository->push()) {
            $logger->notice('Pushed to remote repository');
        }

        return 0;
    }
}
