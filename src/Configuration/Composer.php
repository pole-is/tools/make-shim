<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Configuration;

use Irstea\MakeShim\Packagist\Package;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\NodeInterface;
use Symfony\Component\Config\Definition\Processor;

/**
 * Class Composer.
 */
class Composer
{
    /** @var NodeInterface|null */
    private $configTree;

    /** @var Processor|null */
    private $processor;

    /**
     * @param array $config
     *
     * @return Package
     */
    public function process(array $config): Package
    {
        return Package::fromConfiguration(
            $this->getProcessor()->process(
                $this->getConfigTree(),
                [$config],
                )
        );
    }

    /**
     * @return NodeInterface
     */
    private function getConfigTree(): NodeInterface
    {
        if ($this->configTree === null) {
            return $this->configTree = $this->getConfigTreeBuilder()->buildTree();
        }

        return $this->configTree;
    }

    /**
     * @return Processor
     */
    private function getProcessor(): Processor
    {
        if ($this->processor === null) {
            return $this->processor = new Processor();
        }

        return $this->processor;
    }

    /**
     * @return TreeBuilder
     */
    private function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('composer');

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        /* @noinspection NullPointerExceptionInspection */
        $rootNode
            ->ignoreExtraKeys()
            ->children()
            ->scalarNode('name')->isRequired()->end()
            ->scalarNode('version')->isRequired()->end()
            ->scalarNode('type')->isRequired()->end()
            ->scalarNode('homepage')->end()
            ->arrayNode('keywords')->scalarPrototype()->end()->end()
            ->arrayNode('authors')->variablePrototype()->end()->end()
            ->arrayNode('license')
            ->beforeNormalization()->castToArray()->end()
            ->scalarPrototype()->end()
            ->end()
            ->arrayNode('bin')
            ->isRequired()
            ->requiresAtLeastOneElement()
            ->scalarPrototype()->end()
            ->end()
            ->arrayNode('require')
            ->normalizeKeys(false)
            ->beforeNormalization()
            ->always(\Closure::fromCallable([$this, 'keepPlatformRequirements']))
            ->end()
            ->scalarPrototype()->end()
            ->end()
            ->arrayNode('suggest')
            ->normalizeKeys(false)
            ->beforeNormalization()
            ->always(\Closure::fromCallable([$this, 'keepPlatformRequirements']))
            ->end()
            ->scalarPrototype()->end()
            ->end()
            ->arrayNode('replace')
            ->normalizeKeys(false)
            ->scalarPrototype()->end()
            ->end()
            ->arrayNode('conflicts')
            ->normalizeKeys(false)
            ->beforeNormalization()
            ->always(\Closure::fromCallable([$this, 'keepPlatformRequirements']))
            ->end()
            ->scalarPrototype()->end()
            ->end()
            ->end();

        return $treeBuilder;
    }

    /**
     * @param array<string, string> $requirements
     *
     * @return array<string, string>
     */
    public function keepPlatformRequirements(array $requirements): array
    {
        $result = [];
        foreach ($requirements as $package => $constraint) {
            if ($package === 'php' || strpos($package, 'ext-') === 0) {
                $result[$package] = $constraint;
            }
        }

        return $result;
    }
}
