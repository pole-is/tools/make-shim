<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Configuration;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Yaml\Yaml;

/**
 * Class MakeShim.
 */
class MakeShim implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('make-shim');

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        /* @noinspection NullPointerExceptionInspection */
        $rootNode
            ->children()
                ->arrayNode('package')
                    ->isRequired()
                    ->children()
                        ->scalarNode('name')
                            ->isRequired()
                        ->end()
                        ->scalarNode('versions')
                            ->defaultValue('*')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('phar')
                    ->isRequired()
                    ->children()
                        ->scalarNode('archive_url')->isRequired()->end()
                        ->scalarNode('signature_url')->defaultNull()->end()
                        ->arrayNode('keys')
                            ->defaultValue([])
                            ->beforeNormalization()
                                ->castToArray()
                            ->end()
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('target')
                    ->isRequired()
                    ->children()
                        ->scalarNode('name')
                            ->isRequired()
                        ->end()
                        ->scalarNode('repository')
                            ->defaultNull()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    /**
     * @param string $path
     *
     * @return array
     */
    public function fromFile(string $path): array
    {
        return (new Processor())->processConfiguration($this, [Yaml::parseFile($path)]);
    }
}
