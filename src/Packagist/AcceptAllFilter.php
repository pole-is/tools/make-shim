<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Packagist;

/**
 * Class AcceptAllFilter.
 */
final class AcceptAllFilter implements PackageFilterInterface
{
    /**
     * {@inheritdoc}
     */
    public function filter(array $packages): array
    {
        return $packages;
    }
}
