<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Packagist;

use Assert\Assert;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Irstea\MakeShim\Configuration;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Safe\Exceptions\JsonException;
use function Safe\json_decode;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

/**
 * Class Packagist.
 */
final class Packagist implements PackagistInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var string
     */
    private const PACKAGIST_BASE_URL = 'https://repo.packagist.org/p';

    /**
     * @var Configuration\Composer
     */
    private $configuration;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * Packagist constructor.
     *
     * @param ClientInterface        $client
     * @param Configuration\Composer $configuration
     * @param LoggerInterface|null   $logger
     */
    public function __construct(ClientInterface $client, Configuration\Composer $configuration, LoggerInterface $logger = null)
    {
        $this->client = $client;
        $this->configuration = $configuration;
        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * {@inheritdoc}
     */
    public function enumerateVersions(string $packageName): array
    {
        $this->logger->debug('Fetching packagist metadata', ['package' => $packageName]);

        $packages = $this->fetchMetadata($packageName);

        $this->logger->debug(sprintf('Found %d versions', count($packages)));

        return $packages;
    }

    /**
     * @param string $packageName
     *
     * @throws JsonException
     *
     * @return Package[]
     */
    private function fetchMetadata(string $packageName): array
    {
        $uri = sprintf('%s/%s.json', self::PACKAGIST_BASE_URL, $packageName);

        try {
            $response = $this->client->request('GET', $uri);
        } catch (GuzzleException $exception) {
            throw new \RuntimeException(sprintf('could not fetch %s: %s', $uri, $exception->getMessage()));
        }

        $parsed = json_decode($response->getBody()->getContents(), true);
        Assert::that($parsed)->isArray()->keyExists('packages');
        Assert::that($parsed['packages'])->isArray()->keyExists($packageName);

        $metadata = [];
        foreach ($parsed['packages'][$packageName] as $key => $package) {
            try {
                $metadata[] = $this->configuration->process($package);
            } catch (InvalidConfigurationException $exception) {
                $this->logger->warning(sprintf('Ignoring invalid version %s: %s', $key, $exception->getMessage()));
            }
        }

        return $metadata;
    }
}
