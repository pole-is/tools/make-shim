<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Packagist;

use Composer\Semver\Constraint\Constraint;
use Composer\Semver\VersionParser;

/**
 * Class VersionFilter.
 */
class VersionFilter implements PackageFilterInterface
{
    /**
     * @var string
     */
    private $constraint;
    /**
     * @var VersionParser
     */
    private $versionParser;

    /**
     * VersionFilter constructor.
     *
     * @param string             $constraint
     * @param VersionParser|null $versionParser
     */
    public function __construct(string $constraint, VersionParser $versionParser = null)
    {
        $this->constraint = $constraint;
        $this->versionParser = $versionParser ?: new VersionParser();
    }

    /**
     * {@inheritdoc}
     */
    public function filter(array $packages): array
    {
        $constraint = $this->versionParser->parseConstraints($this->constraint);

        $accepted = [];
        foreach ($packages as $package) {
            $version = $this->versionParser->normalize($package->getVersion());

            if (VersionParser::parseStability($version) !== 'stable') {
                continue;
            }
            $provider = new Constraint('==', $version);
            if (!$constraint->matches($provider)) {
                continue;
            }
            $accepted[] = $package;
        }

        usort($accepted, [Package::class, 'compareByVersion']);

        return $accepted;
    }
}
