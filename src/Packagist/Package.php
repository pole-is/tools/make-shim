<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Packagist;

use Composer\Semver\Comparator;

/**
 * Class Package.
 */
final class Package
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $version;
    /**
     * @var string
     */
    private $binary;
    /**
     * @var array
     */
    private $configuration;

    /**
     * Version constructor.
     *
     * @param string $name
     * @param string $version
     * @param string $binary
     * @param array  $configuration
     */
    public function __construct(string $name, string $version, string $binary, array $configuration)
    {
        $this->name = $name;
        $this->version = $version;
        $this->binary = $binary;
        $this->configuration = $configuration;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get version.
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * Get binary.
     *
     * @return string
     */
    public function getBinary(): string
    {
        return $this->binary;
    }

    /**
     * Get configuration.
     *
     * @return array
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    /**
     * @param array $configuration
     *
     * @return Package
     */
    public static function fromConfiguration(array $configuration): self
    {
        return new self(
            $configuration['name'],
            $configuration['version'],
            $configuration['bin']['0'],
            $configuration
        );
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s:%s', $this->name, $this->version);
    }

    /**
     * @param Package $first
     * @param Package $second
     *
     * @return int
     */
    public static function compareByVersion(Package $first, Package $second): int
    {
        if (Comparator::lessThan($first->getVersion(), $second->getVersion())) {
            return -1;
        }
        if (Comparator::greaterThan($first->getVersion(), $second->getVersion())) {
            return 1;
        }

        return 0;
    }
}
