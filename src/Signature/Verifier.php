<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Signature;

use Assert\Assertion;
use Irstea\MakeShim\Exception\VerifierException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Process\Exception\ExceptionInterface;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;

/**
 * Class Verifier.
 */
class Verifier implements VerifierInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var string
     */
    private $gpgBinary;

    /**
     * @var string[]
     */
    private $trustedKeys = [];

    /**
     * Verifier constructor.
     *
     * @param string               $gpgBinary
     * @param LoggerInterface|null $logger
     */
    public function __construct(string $gpgBinary = 'gpg', LoggerInterface $logger = null)
    {
        $this->gpgBinary = $gpgBinary;
        $this->setLogger($logger ?: new NullLogger());
    }

    /**
     * {@inheritdoc}
     */
    public function loadKeys(array $ids): void
    {
        $this->trustedKeys = array_merge($this->trustedKeys, $ids);
    }

    /**
     * {@inheritdoc}
     */
    public function verify(string $signaturePath, string $dataPath): void
    {
        try {
            Assertion::file($signaturePath);
            Assertion::file($dataPath);

            $cmd = array_merge(
                [$this->getGpgBinaryPath()],
                $this->getGpgOptions(),
                ['--verify', $signaturePath, $dataPath]
            );

            $process = new Process($cmd);
            $process->mustRun();
            $this->logger->info("$dataPath signature verified.");
        } catch (ExceptionInterface $exception) {
            throw new VerifierException("could not verify $dataPath signature: " . $exception->getMessage(), 0, $exception);
        }
    }

    /**
     * @return array
     */
    private function getGpgOptions(): array
    {
        $options = [
            '--batch',
            '--auto-key-locate=wkd',
            '--auto-key-retrieve',
        ];

        foreach ($this->trustedKeys as $keyId) {
            $options[] = '--trusted-key';
            $options[] = $keyId;
        }

        return $options;
    }

    /**
     * @return string
     */
    private function getGpgBinaryPath(): string
    {
        if (\is_executable($this->gpgBinary)) {
            return $this->gpgBinary;
        }
        $finder = new ExecutableFinder();
        $found = $finder->find($this->gpgBinary);
        if (!$found) {
            throw new VerifierException('binary not found: ' . $this->gpgBinary);
        }
        $this->logger->debug("gpg path: $found");

        return $this->gpgBinary = $found;
    }
}
