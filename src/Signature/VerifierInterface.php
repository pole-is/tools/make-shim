<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Signature;

use Irstea\MakeShim\Exception\VerifierException;

/**
 * Interface VerifierInterface.
 */
interface VerifierInterface
{
    /**
     * @param string[] $ids
     *
     * @throws VerifierException
     */
    public function loadKeys(array $ids): void;

    /**
     * @param string $signaturePath
     * @param string $dataPath
     *
     * @throws VerifierException
     */
    public function verify(string $signaturePath, string $dataPath): void;
}
