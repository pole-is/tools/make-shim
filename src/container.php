<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/*
 * Copyright (C) 2019 IRSTEA
 * All rights reserved.
 *
 * @copyright 2019 IRSTEA
 * @author guillaume.perreal
 */

namespace Irstea\MakeShim;

use Doctrine\Common\Cache\FilesystemCache;
use GitWrapper\GitWrapper;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Irstea\MakeShim\Builder\Builder;
use Irstea\MakeShim\Command\BuildCommand;
use Irstea\MakeShim\Configuration\Composer;
use Irstea\MakeShim\Configuration\MakeShim;
use Irstea\MakeShim\Packagist\Packagist;
use Irstea\MakeShim\Packagist\VersionFilter;
use Irstea\MakeShim\Repository\Factory;
use Irstea\MakeShim\Signature\NullVerifier;
use Irstea\MakeShim\Signature\Verifier;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Storage\DoctrineCacheStorage;
use Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy;
use PackageVersions\Versions;
use Pimple\Container;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Webmozart\PathUtil\Path;
use XdgBaseDir\Xdg;

$container = new Container();

$container['application'] = static function (Container $c) {
    $application = new Application('make-shim', Versions::getVersion('irstea/make-shim'));
    $application->add($c['build-command']);
    $application->setDefaultCommand($c['build-command']->getName(), true);

    return $application;
};

$container['build-command'] = static function (Container $c) {
    return new BuildCommand($c);
};

$container['packagist'] = static function (Container $c) {
    return new Packagist(
        $c['http-client'],
        new Composer(),
        $c['logger']
    );
};

$container['package-filter'] = static function (Container $c) {
    return new VersionFilter($c['config']['package']['versions']);
};

$container['http-client'] = static function (Container $c) {
    $stack = HandlerStack::create();

    $stack->after('allow_redirects', new CacheMiddleware(new PrivateCacheStrategy(
        new DoctrineCacheStorage($c['cache'])
    )), 'cache');

    $stack->push($c['http-log-middleware']);

    return new Client(
        [
            'handler'         => $stack,
            'allow_redirects' => true,
        ]
    );
};

$container['http-log-middleware'] = $container->factory(static function (Container $c) {
    static $serial = 0;
    $logger = $c['logger'];
    $id = ++$serial;

    return static function (callable $handler) use ($logger, $id) {
        return static function (RequestInterface $request, array $options) use ($handler, $logger, $id) {
            $logger->debug(sprintf('#%d: HTTP >> %s %s', $id, $request->getMethod(), $request->getUri()));

            return $handler($request, $options)->then(
                function (ResponseInterface $response) use ($logger, $id) {
                    $size = $response->getBody()->getSize();
                    $logger->debug(sprintf(
                        '#%d: HTTP << %d %s - %s (%d bytes)',
                        $id,
                        $response->getStatusCode(),
                        $response->getReasonPhrase(),
                        implode('; ', $response->getHeader('Content-Type')),
                        $size ? (string) $size : '???'
                    ));

                    return $response;
                }
            );
        };
    };
});

$container['logger'] = static function (Container $c) {
    return new ConsoleLogger($c['output']);
};

$container['builder'] = static function (Container $c) {
    return new Builder(
        $c['config']['target']['name'],
        $c['build-path'],
        $c['config']['phar']['archive_url'],
        $c['config']['phar']['signature_url'],
        $c['http-client'],
        $c['verifier'],
        $c['logger']
    );
};

$container['build-path'] = static function (Container $c) {
    return Path::join('build', $c['config']['package']['name']);
};

$container['config'] = static function (Container $c) {
    return (new MakeShim())->fromFile($c['config-path']);
};

$container['repository-factory'] = static function (Container $c) {
    return new Factory(
        new GitWrapper(),
        $c['logger']
    );
};

$container['repository'] = static function (Container $c) {
    return $c['repository-factory']->create($c['build-path'], $c['no-remote'] ? null : $c['config']['target']['repository']);
};

$container['verifier'] = static function (Container $c) {
    if ($c['no-verify']) {
        return new NullVerifier();
    }

    $keys = $c['config']['phar']['keys'];
    if (!$keys) {
        return new NullVerifier();
    }

    $verifier = new Verifier($c['gpg-path'], $c['logger']);
    $verifier->loadKeys($keys);

    return $verifier;
};

$container['cache'] = function (Container $c) {
    return new FilesystemCache($c['cache-path']);
};

$container['cache-path'] = function (Container $c) {
    $cachePath = \getenv('MAKE_SHIM_CACHE');
    if ($cachePath) {
        return $cachePath;
    }

    return Path::join((new Xdg())->getHomeCacheDir(), 'make-shim', 'http');
};

$container['gpg-path'] = 'gpg';

return $container;
