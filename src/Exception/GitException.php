<?php declare(strict_types=1);
/*
 * This file is part of "irstea/make-shim".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Irstea\MakeShim\Exception;

/**
 * Class GitException.
 */
class GitException extends \RuntimeException implements Exception
{
    /**
     * @param \GitWrapper\GitException $gitException
     *
     * @return GitException
     */
    public static function fromGitWrapper(\GitWrapper\GitException $gitException): self
    {
        return new self($gitException->getMessage(), 0, $gitException);
    }
}
